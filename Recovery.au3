;#=#INDEX#==================================================================#
;#  Title .........: Recovery.au3                                           #
;#  Description....: AutoIt3 Recovery                                       #
;#  Date ..........: 2019-03-24                                             #
;#  Authors .......: Piotr Wiśniewski ©makeitright.pl                       #
;#                   Inspiration by: jennico (jennicoattminusonlinedotde)   #
;#                                   AutoIT forum                           #
;#==========================================================================#

#include-once
#include <Array.au3>
#include <ScreenCapture.au3>
#include <Json.au3>

_OnAutoItError()

Local $Recovery_err



;   this function is made to be customized !
Global $licznik

Func _OnAutoItError()
	Local $Recovery_sMsg = ""
	If StringInStr($CmdLineRaw,"/AutoIt3ExecuteScript") Then Return
	Local $Recovery_iPID = Run(@AutoItExe&' /AutoIt3ExecuteScript "'&@ScriptFullPath&'"',@ScriptDir,0,6)
    ProcessWait($Recovery_iPID)
	Opt("TrayIconHide",1)	; hidden icon Recovery in tray
	While 1
		$Recovery_sMsg = StdoutRead($Recovery_iPID)
		ConsoleWrite($Recovery_sMsg)
		$Recovery_sMsg = ""
		If WinExists("[TITLE:AutoIt Error; CLASS:#32770]") Then ExitLoop ; Error in Script Main then
		If NOT ProcessExists($Recovery_iPID) Then Exit ; End Script Main No error then End Recovery
		Sleep(200)
	WEnd
	__EndScriptError()
	Exit
EndFunc



; #INTERNAL_USE_ONLY# ===========================================================================================================
; Name ..........: __EndScriptError
; Description ...: Obsługa okna komunikatu przerwania skryptu z błędem wykonania
; Syntax ........: __EndScriptError()
; Parameters ....: None
; Return values .: None
; Author ........: Piotr Wiśniewski ©makeitright.pl
; Modified ......:
; Remarks .......:
; Related .......:
; Link ..........:
; Example .......: No
; ===============================================================================================================================
Func __EndScriptError()
	Local $Recovery_FileResult
	Local $Recovery_FileScreen
	Local $Recovery_sError
	Local $Recovery_oJSONResult
	If StringInStr(@ScriptDir, "\workspace\", $STR_NOCASESENSE) > 0 Then
		;exists WORKSPACE
		$Recovery_FileResult = StringLeft(@ScriptDir,StringInStr(@ScriptDir,"\workspace\") + 10) & "result\result.json"
		$Recovery_FileScreen =  StringLeft(@ScriptDir,StringInStr(@ScriptDir,"\workspace\") + 10) & "result\attachments\ERROR.png"
	Else
		;NOT exists WORKSPACE
		$Recovery_FileResult = @ScriptDir & "\result.json"
		$Recovery_FileScreen = @ScriptDir & "\ERROR.png"
	EndIf

	If WinExists("[TITLE:AutoIt Error; CLASS:#32770]") Then
		Sleep(500)
		$Recovery_sError = ControlGetText("[TITLE:AutoIt Error; CLASS:#32770]", "", "[CLASS:Static; INSTANCE:2]") & @LF & "++++++++++++++++++++++++++++" & @LF & "Dołączono screen okna błędu."
		_ScreenCapture_CaptureWnd($Recovery_FileScreen, "[TITLE:AutoIt Error; CLASS:#32770]")
		ControlClick("[TITLE:AutoIt Error; CLASS:#32770]", "", "[CLASS:Button; INSTANCE:1]", "left")
	Else
		$Recovery_sError = "BRAK DANYCH"
	EndIf
	Json_Put($Recovery_oJSONResult, ".status", "failure")
	Json_Put($Recovery_oJSONResult, ".result", "AWARIA SKRYPTU " & $Recovery_sError)
	Json_Put($Recovery_oJSONResult, ".error.stack_trace", @ScriptName)
	Json_Put($Recovery_oJSONResult, ".error.caused_by", $Recovery_sError)
	Json_Put($Recovery_oJSONResult, ".error.screenshot", "")
	Local $Recovery_hFileResult = FileOpen($Recovery_FileResult, 258)
	FileWrite($Recovery_hFileResult, Json_Encode($Recovery_oJSONResult, $JSON_UNESCAPED_UNICODE))
	FileClose($Recovery_hFileResult)

EndFunc










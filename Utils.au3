﻿#include-once
#include <Main.au3>
#include <Array.au3>

Local $oMyError = Null	; zmienna techniczna




; #FUNCTION# ====================================================================================================================
; Name ..........: _Utils_Msg
; Description ...: Funkcja wyświetlająca okienko komunikatu z przyciskami [TAK] i [NIE] ułatwia analizę skryptu i przerwanie działania skryptu
; Syntax ........: _Utils_Msg($naglowek, $Utils_Tresc)
; Parameters ....: $naglowek            - a general number value.
;                  $Utils_Tresc               - a dll struct value.
; Return values .: None
; Author ........: Piotr Wiśniewski © makeitright.pl
; ===============================================================================================================================
Func _Utils_Msg($Utils_Title="",$Utils_Contents="")
	$Utils_Contents = $Utils_Contents & @CRLF & @CRLF & "[TAK] - wykonano i lecimy dalej" & @CRLF & "[NIE] - PRZERWANIE działania programu"
	If Not IsDeclared("iMsgBoxAnswer") Then Local $Utils_iMsgBoxAnswer
	$Utils_iMsgBoxAnswer = MsgBox(52,$Utils_Title,$Utils_Contents)
	Select
		Case $Utils_iMsgBoxAnswer = 6 ;Yes

		Case $Utils_iMsgBoxAnswer = 7 ;No
			Exit
	EndSelect
EndFunc   ; ==> _Utils_Msg()



; #FUNCTION# ====================================================================================================================
; Name ..........: _Utils_VarTo2D
; Description ...: Funkcja konwertuje dane tekstowe na tablice, kolejne rekordy to wiersze oddzielone @CRLF
; Syntax ........: _Utils_VarTo2D($Utils_Var[, $Utils_sSeparator = "|"])
; Parameters ....: $Utils_Var                 - a variant value.
;                  $Utils_sSeparator          - [optional] a string value. Default is "|".
; Return values .: Tablica $aResult
; Author ........: WWW
; ===============================================================================================================================
Func _Utils_VarTo2D($Utils_Var, $Utils_sSeparator = "|")
    Local $aRows = StringSplit(StringStripCR($Utils_Var), @CRLF), $Utils_aColumns, $aResult[$aRows[0]][1]
    For $Utils_iRow = 1 To $aRows[0]
        $Utils_aColumns = StringSplit($aRows[$Utils_iRow], $Utils_sSeparator)
        If $Utils_aColumns[0] > UBound($aResult, 2) Then _
			ReDim $aResult[$aRows[0]][$Utils_aColumns[0]]
        For $Utils_iColumn = 1 To $Utils_aColumns[0]
            $aResult[$Utils_iRow - 1][$Utils_iColumn - 1] = $Utils_aColumns[$Utils_iColumn]
        Next
    Next
    Return $aResult
EndFunc   ; ==> _Utils_VarTo2D()

; #FUNCTION# ====================================================================================================================
; Name ..........: _Utils_CW
; Description ...: Wypisanie na konsoli podanego tekstu z przejściem do nowej linii i dodaniem extra linii dla następnego wpisu w konsoli.
; Syntax ........: _Utils_CW($msg)
; Parameters ....: $msg		- treść do wpisania na konsolę (kody kolorystyczne: ! red  > blue  - orange  + green)
; Return values .: None
; Author ........: Mateusz Kłosowski © Makeitright
; ===============================================================================================================================
Func _Utils_CW($Utils_Msg)
	ConsoleWrite($Utils_Msg & @CRLF & @CRLF)
EndFunc   ; ==> _Utils_CW


; #FUNCTION# ====================================================================================================================
; Name ..........: _Utils_ReplacePolishCharacters
; Description ...: Zamiana polskiech znaków w podanym stringu.
; Syntax ........: _Utils_ReplacePolishCharacters($Utils_sString)
; Parameters ....: $Utils_sString		- tekst do zmiany
; Return values .: tekst z zamienionymi polskimi znakami.
; Author ........: Mateusz Kłosowski © Makeitright
; ===============================================================================================================================
Func _Utils_ReplacePolishCharacters($Utils_sString)
	Local $Utils_aPLstripUpper[18] = [ "Ą", "A", "Ć", "C", "Ę", "E", "Ł", "L", "Ń", "N", "Ó", "O", "Ś", "S", "Ź", "Z", "Ż", "Z" ]
	Local $Utils_aPLstripLower[18] = [ "ą", "a", "ć", "c", "ę", "e", "ł", "l", "ń", "n", "ó", "o", "ś", "s", "ź", "z", "ż", "z" ]
	For $Utils_i = 0 To UBound($Utils_aPLstripUpper) -1 Step 2
		$Utils_sString = StringReplace($Utils_sString, $Utils_aPLstripUpper[$Utils_i], $Utils_aPLstripUpper[$Utils_i+1], 0, $STR_CASESENSE)
	Next
	For $Utils_i = 0 To UBound($Utils_aPLstripLower) -1 Step 2
		$Utils_sString = StringReplace($Utils_sString, $Utils_aPLstripLower[$Utils_i], $Utils_aPLstripLower[$Utils_i+1], 0, $STR_CASESENSE)
	Next
	Return $Utils_sString
EndFunc   ; ==> _Utils_ReplacePolishCharacters

; #FUNCTION# ====================================================================================================================
; Name ..........: _Utils_StringNameCase
; Description ...: Zamiana pierwszych liter w podanym ciągu znaków na wielkie.
;					Funkcja powstała na bazie funkcji _StringTitleCase()
; Syntax ........: _Utils_StringNameCase($Utils_sString)
; Parameters ....: $Utils_sString		- ciąg znaków do konwersji
; Return values .: ciąg znaków sformatowanych "Jak W Nazwie  Własnej"
; Author ........: Mateusz Kłosowski © Makeitright
; ===============================================================================================================================
Func _Utils_StringNameCase($Utils_sString)
	Local $Utils_bCapNext = True, $Utils_sChr = "", $Utils_sReturn = ""
	$Utils_sString = StringLower($Utils_sString)
	For $Utils_i = 1 To StringLen($Utils_sString)
		$Utils_sChr = StringMid($Utils_sString, $Utils_i, 1)
		If $Utils_sChr = " " Then
			$Utils_bCapNext = True
		ElseIf $Utils_bCapNext Then
			$Utils_sChr = StringUpper($Utils_sChr)
			$Utils_bCapNext = False
		EndIf
		$Utils_sReturn &= $Utils_sChr
	Next
	Return $Utils_sReturn
EndFunc		; ==> _Utils_StringNameCase()


; #FUNCTION# ====================================================================================================================
; Name ..........: _GetUserFullData
; Description ...: Pobieranie danych zalogowanego użytkownika z ActiveDirectory.
; Syntax ........: _GetUserFullData()
; Parameters ....: None
; Return values .: tablica z odczytanymi danymi: [ 0:email | 1:imię | 2:nazwisko | 3:pełna nazwa ]
; Author ........: www
; ===============================================================================================================================
Func _GetUserFullData()
	$oMyError = ObjEvent("AutoIt.Error", "ComError")
;	Local $objRootDSE = ObjGet("LDAP://RootDSE")
	Local $objTrans = ObjCreate("NameTranslate")
	$objTrans.Init (3, "")
	$objTrans.Set (1, @LogonDomain)
	$objTrans.Set (3, @LogonDomain & "\" & @UserName)
	Local $strUserDN = $objTrans.Get (1)
	Local $UserObj = ObjGet("LDAP://" & $strUserDN)
	Local $emailaddress		= $UserObj.EmailAddress
	Local $firstname		= $UserObj.FirstName
	Local $lastname			= $UserObj.LastName
	Local $fullname			= $UserObj.FullName
;~ 	Local $login			= @UserName
;~ 	Local $department		= $UserObj.Department
;~ 	Local $streetaddress	= $UserObj.get("streetAddress")
;~ 	Local $city				= $UserObj.get("l")
;~ 	Local $state			= $UserObj.get("st")
;~ 	Local $zipcode			= $UserObj.PostalCodes
;~ 	Local $country			= $UserObj.get("c")
;~ 	Local $officenumber		= $UserObj.TelephoneNumber
;~ 	Local $mobilenumber		= $UserObj.TelephoneMobile
;~ 	Local $faxnumber		= $UserObj.FaxNumber
;~ 	Local $homeMDB			= $UserObj.get("homeMDB")
;~ 	Local $homeMDBtext		= StringSplit($homeMDB, ",")
;~ 	MsgBox(0, "AD", $firstname & " " & $lastname & " [" & $fullname & "]" & " - " & $login &  @CRLF & $streetaddress & @CRLF & $zipcode & " " & $city & " (" & $state & ") " & " - " & $country & @CRLF & $emailaddress)
	Local $return[4] = [ $emailaddress, $firstname, $lastname, $fullname ]
	$oMyError = ObjEvent("AutoIt.Error", "")
	Return $return
EndFunc		; ==> _GetUserFullData()

; COM Error handler dla funkcji _GetUserFullData()
Func ComError()
	If IsObj($oMyError) Then
		Local $HexNumber = Hex($oMyError.number, 8)
		Return SetError($HexNumber, 0, "Błąd pobierania danych")
	EndIf
	Return SetError(1, 0, "Błąd inicjalizacji obiektu")
EndFunc

; #FUNCTION# ====================================================================================================================
; Name ..........: _Utils_DeCrypt
; Description ...: Deszyfrowanie hasła z argumentu używając klucza z pliku.
; Syntax ........: _Utils_DeCrypt($Utils_Haslo)
; Parameters ....:	$sciezkaPlikKey		- ścieżka do pliku z kluczem
;					$Utils_Haslo				- zaszyfrowane hasło
; Return values .: hasło jawne lub @error z komunikatem błędu
; Author ........: Mateusz Kłosowski © Makeitright
; ===============================================================================================================================
Func _Utils_DeCrypt($Utils_SciezkaPlikHash, $Utils_Haslo)
	; biblioteka szyfrująca
	_Crypt_Startup()
	If @error Then _
		Return SetError(1, 0, _Main_LoggerError($Main_G_sMsgWorkerError, @ScriptName & " / standard / _DeCrypt()", "Błąd uruchamiania biblioteki szyfrowania.", "br"))
	; klucz do deszyfrowania z pliku
	Local $hKey = FileRead($Utils_SciezkaPlikHash)
	If @error Then _
		Return SetError(2, 0, _Main_LoggerError($Main_G_sMsgWorkerError, @ScriptName & " / standard / _DeCrypt()", "Błąd odczytu pliku z kluczem szyfrowania na stacji wykonawczej.", "br"))
	; odszyfrowanie hasła z użyciem klucza z pliku
	Local $Utils_HasloOdkodowaneBinary = _Crypt_DecryptData($Utils_Haslo, $hKey, $CALG_AES_256)
	If @error Then _
		Return SetError(3, 0, _Main_LoggerError($Main_G_sMsgWorkerError, @ScriptName & " / standard / _DeCrypt()", "Błąd rozszyfrowania hasła.", "br"))
	_Crypt_Shutdown()
	Return BinaryToString($Utils_HasloOdkodowaneBinary)
EndFunc		; ==> _Utils_DeCrypt()


; #FUNCTION# ====================================================================================================================
; Name ..........: _GetUserFullName
; Description ...: Pobranie pełnej nazwy zalogowanego na stacji użytkownika z katalogu AD.
; Syntax ........: _GetUserFullName()
; Parameters ....: None
; Return values .: Nazwa użytkownika lub @error i pusty string.
; Author ........: www
; ===============================================================================================================================
Func _GetUserFullName()
    Local $colItems = ""
    Local $objWMIService = ObjGet("winmgmts:\\localhost\root\CIMV2")
    $colItems = $objWMIService.ExecQuery("SELECT * FROM Win32_UserAccount WHERE Name = '" & @UserName &  "'", "WQL", 0x10 + 0x20)
	If IsObj($colItems) Then
		For $objItem In $colItems
			Return $objItem.FullName
		Next
	Endif
	Return SetError(1, 0, "")
EndFunc		; ==> _GetUserFullName()

; #FUNCTION# ====================================================================================================================
; Name ..........: _ModArray
; Description ...: Ustawienie wartości będącej w podanej tablicy w podanym indexie. Funkcja przydatna gdy trzeba zmodyfikować wartość w tablicy, która jest wewnątrz innej tablicy.
; Syntax ........: _ModArray($aArray, $Utils_iIndex, $value)
; Parameters ....: $aArray	- tablica do zmodyfikowania
;                  $Utils_iIndex	- index w tablicy
;                  $value	- nowa wartość
; Return values .: None
; Author ........: Mateusz Kłosowski © Makeitright
; ===============================================================================================================================
Func _ModArray(ByRef $aArray, $Utils_iIndex, $value)
	$aArray[$Utils_iIndex] = $value
EndFunc		; ==> _ModArray()


; #FUNCTION# ====================================================================================================================
; Name ..........: _Utils_ValidateDate
; Description ...: Walidacja czy podany string jest w określonym w argumencie formacie daty
; Syntax ........: _Utils_ValidateDate($Utils_DataDoWalidacji, $Utils_FormatDaty = "yyyy-MM-dd")
; Parameters ....: $Utils_DataDoWalidacji     - string do walidacji czy jest datą w odpowiednim formacie
;				   $Utils_FormatDaty			- walidowany format daty, domyślnie "yyyy-MM-dd"
; Return values .: Data w formacie określonym w argumencie $Utils_FormatDaty.
;					Jeżeli jest błąd ustawiana jest wartość @error i zwracany komunikat błędu.
; Author ........: Mateusz Kłosowski © makeitright.pl
; ===============================================================================================================================
Func _Utils_ValidateDate($Utils_DataDoWalidacji, $Utils_FormatDaty = "yyyy-MM-dd")
	Local $Utils_KomunikatBleduWalidacji = "Nieprawidłowy format podanej daty (" & $Utils_DataDoWalidacji & "): "
	Local $Utils_Data = StringStripWS($Utils_DataDoWalidacji, 8)
	Switch $Utils_FormatDaty
		Case "yyyy-MM-dd"
;~ 			Local $Utils_aDataStanu = StringSplit(StringReplace(StringReplace(StringReplace(StringReplace($Utils_Data,":","-"),".","-"),"/","-"), "\", "-"), "-")
			$Utils_Data = StringRegExpReplace($Utils_Data, ":+|\.+|/+|\\+", "-")
			Local $Utils_aDataStanu = StringSplit($Utils_Data, "-")
			Select
				Case StringLen($Utils_Data) = 0
					Return SetError(2, 0, $Utils_KomunikatBleduWalidacji & "brak podanej daty (oczekiwano daty w formacie 'yyyy-MM-dd').")
				Case StringLen($Utils_Data) <> 10
					Return SetError(3, 0, $Utils_KomunikatBleduWalidacji & "oczekiwano 10 znaków (data powinna być w formacie 'yyyy-MM-dd').")
				Case $Utils_aDataStanu[0] < 3
					Return SetError(4, 0, $Utils_KomunikatBleduWalidacji & "data powinna być w formacie 'yyyy-MM-dd'.")
				Case Not (StringIsDigit($Utils_aDataStanu[1]) And StringIsDigit($Utils_aDataStanu[2]) And StringIsDigit($Utils_aDataStanu[3]))
					Return SetError(5, 0, $Utils_KomunikatBleduWalidacji & "data zawiera nieprawidłowe znaki - obsługiwane są tylko cyfry i separator '-'.")
				Case StringLen($Utils_aDataStanu[1]) < 4
					Return SetError(6, 0, $Utils_KomunikatBleduWalidacji & "rok powinien być 4-cyfrowy (data powinna być w formacie 'yyyy-MM-dd').")
				Case StringLen($Utils_aDataStanu[2]) < 2
					Return SetError(7, 0, $Utils_KomunikatBleduWalidacji & "miesiąc powinien być 2-cyfrowy, z poprzedzającym zerem dla miesięcy 1-9 (data powinna być w formacie 'yyyy-MM-dd').")
				Case StringLen($Utils_aDataStanu[3]) < 2
					Return SetError(8, 0, $Utils_KomunikatBleduWalidacji & "dzień powinien być 2-cyfrowy, z poprzedzającym zerem dla dni 1-9 (data powinna być w formacie 'yyyy-MM-dd').")
				Case Number($Utils_aDataStanu[1]) < 1900 Or Number($Utils_aDataStanu[1]) > @YEAR
					Return SetError(9, 0, $Utils_KomunikatBleduWalidacji & "rok mniejszy niz 1900 i większy niż aktualny (" & @YEAR & ").")
				Case Number($Utils_aDataStanu[2]) < 1 Or Number($Utils_aDataStanu[2]) > 12
					Return SetError(10, 0, $Utils_KomunikatBleduWalidacji & "miesiąc mniejszy niż 1 i większy niż 12.")
				Case Number($Utils_aDataStanu[3]) < 1 Or _DateDaysInMonth(Number($Utils_aDataStanu[1]), Number($Utils_aDataStanu[2])) < Number($Utils_aDataStanu[3])
					Return SetError(11, 0, $Utils_KomunikatBleduWalidacji & "nieprawidłowa liczba dni w miesiącu.")
			EndSelect
		Case Else
			Return SetError(1, 0, "Nieprawidłowy argument funkcji walidującej format daty.")
	EndSwitch
	; walidacja bezbłędna
	Return $Utils_Data
EndFunc		; ==> _Utils_ValidateDate

; #FUNCTION# ====================================================================================================================
; Name ..........: _LiczbaPoPrzecinku
; Description ...: Obcina string do wskazanych miejsc po przecinku
; Syntax ........: _LiczbaPoPrzecinku($Utils_Liczba[, $Utils_Miejsca = "2"])
; Parameters ....: $Utils_Liczba - dowolny string zawierający TYLKO 1 przecinek
;                  $Utils_Miejsca - [optional] a map. Default is "2".
; Return values .: Nowa wartość lub False
; Author ........:  Piotr Wiśniewski © makeitright.pl
; ===============================================================================================================================
Func _LiczbaPoPrzecinku($Utils_Liczba, $Utils_Miejsca="2")
	If $Utils_Liczba <> "" Then
		StringReplace($Utils_Liczba, ",", ",")
		If @extended > 1 Then
			Return False
		ElseIf @extended = 0 Then
			Return $Utils_Liczba
		EndIf
		Local $PozycjaPrzecinka = StringInStr($Utils_Liczba,",")
		If $PozycjaPrzecinka > StringLen(StringTrimLeft($Utils_Liczba,$PozycjaPrzecinka)) Then _
			Return False
		If StringLen($Utils_Liczba) - $PozycjaPrzecinka >= $Utils_Miejsca Then _
			Return StringLeft($Utils_Liczba, $PozycjaPrzecinka + $Utils_Miejsca)
	Else
		Return $Utils_Liczba
	EndIf
EndFunc		; ==> _LiczbaPoPrzecinku()

; #FUNCTION# ====================================================================================================================
; Name ..........: _Info
; Description ...: Funkcja de debugowania skryptów, wyświetla po lewej stronie SplashText który jest aktualizowany z kadym wywołaniem funkcji
; Syntax ........: _Info($Utils_Tresc)
; Parameters ....: $Utils_Tresc               - string o składni "[treść]=[wartość];[treść_n]=[wartość_n]"
; Return values .: None
; Author ........: Piotr Wiśniewski © makeitright.pl
; ===============================================================================================================================
Func _Info($Utils_Tresc)
	$Utils_Tresc = StringReplace($Utils_Tresc,"=",": ")
	$Utils_Tresc = StringReplace($Utils_Tresc,";",@CRLF)

	If NOT WinExists(@ScriptName) Then
		Local $VirtX = DllCall("user32.dll", "int", "GetSystemMetrics", "int", 78)
		Sleep(500)
		If Not IsKeyword($Main_G_Application) Then _
			SplashTextOn(@ScriptName,$Utils_Tresc,205,600,(($VirtX[0]-15)-(WinGetClientSize($Main_G_Application,"")[0])-220),5,4,"Courier New",9)
	Else
		ControlSetText(@ScriptName,"","Static1",$Utils_Tresc)
	EndIf
EndFunc		; ==> _Info()




; #FUNCTION# ====================================================================================================================
; Name ..........: _Utils_GetHwndFromPID
; Description ...: Funkcja pobiera hWnd okna z PID'u w określonym czasie
; Syntax ........: _Utils_GetHwndFromPID($PID[, $timeout = 10])
; Parameters ....: $PID                 - PID uruchomionej aplikacji.
;                  $timeout             - czas w którym funkcja stara się pobrać hWND
; Return values .: hWND
; Author ........: WWW
; ===============================================================================================================================
Func _Utils_GetHwndFromPID($PID, $timeout = 10)
	Local $Utils_Secs = TimerInit()
	Local $Utils_hWnd = 0
	Local $Utils_WinList = WinList()
	Do
		For $Utils_i = 1 To $Utils_WinList[0][0]
			If $Utils_WinList[$Utils_i][0] <> "" Then
				Local $Utils_iPID2 = WinGetProcess($Utils_WinList[$Utils_i][1])
				If $Utils_iPID2 = $PID Then
					$Utils_hWnd = $Utils_WinList[$Utils_i][1]
					ExitLoop
				EndIf
			EndIf
		Next
	Until ($Utils_hWnd <> 0) Or (TimerDiff($Utils_Secs) >= $timeout * 1000) ;kończy pętlę po znalezieniu hWnd lub czasie timeout w sekundach
	Return $Utils_hWnd
EndFunc	; ==> _Utils_GetHwndFromPID()


; #FUNCTION# ====================================================================================================================
; Name ..........: _ArrayToRichString
; Description ...:
; Syntax ........: _ArrayToRichString(Const Byref $aArray)
; Parameters ....: $aArray              - [in/out and const] an array of unknowns.
; Return values .:          String from Array 1D or 2D
; Author ........: Piotr Wisniewski ©makeitright.pl
; Modified ......:
; Remarks .......:
; Related .......:
; Link ..........:
; Example .......: No
; ===============================================================================================================================
Func _Utils_ArrayToRichString(Const ByRef $aArray)
	If Not IsArray($aArray) Then Return SetError(1, 0, -1)
	Local $iWiersz, $iKolumna, $sWynik=""
	Local $Rozmiar = 0
	Local $Wiersz = UBound($aArray, $UBOUND_ROWS)
	Local $Kolumna = UBound($aArray, $UBOUND_COLUMNS)

	If $Wiersz > 0 And $Kolumna = 0 Then
		Local $aRozmiary[$Wiersz]	; tablica 1D
		For $iWiersz = 0 To $Wiersz-1
			If $Rozmiar < StringLen($aArray[$iWiersz]) Then $Rozmiar = StringLen($aArray[$iWiersz])
		Next
		For $iWiersz = 0 To $Wiersz-1
			$sWynik = $sWynik & "║" & StringFormat("%-" & $Rozmiar & "s", $aArray[$iWiersz]) & "║" & @CRLF
		Next
		Return $sWynik
	ElseIf $Wiersz > 0 And $Kolumna > 0 Then
		Local $aRozmiary[1][$Kolumna]	; tablica 2D
		For $iKolumna = 0 To $Kolumna-1
			$aRozmiary[0] [$iKolumna] = 0	; uzupełniamy tablicę danymi startowymi
		Next
		;pobieramy wartości największych długości pól lecąc 1sza kol wiersze 0 to MAX, 2kol wiersz0 to max
		For $iKolumna = 0 To $Kolumna-1
			For $iWiersz = 0 To $Wiersz-1
				If $aRozmiary[0][$iKolumna] < StringLen($aArray[$iWiersz][$iKolumna]) Then $aRozmiary[0][$iKolumna] = StringLen($aArray[$iWiersz][$iKolumna])
			Next
		Next

		For $iWiersz = 0 To $Wiersz-1
			For $iKolumna = 0 To $Kolumna-1
				If $iKolumna = 0 Then $sWynik = $sWynik & "║"	; początek linijki
				$sWynik = $sWynik & StringFormat("%-" & $aRozmiary[0][$iKolumna] & "s", $aArray[$iWiersz][$iKolumna]) & "║"
			Next
			$sWynik = $sWynik & @CRLF
		Next
		Return $sWynik
	EndIf
EndFunc




;=========================================================
;=========================================================
; SEKCJA TESTOWEGO URUCHAMIANIA FUNKCJI
;=========================================================

If @ScriptName = "add_functions.au3" Then
	Local $Utils_iMsgBoxAnswer
	$Utils_iMsgBoxAnswer = MsgBox(36,"UWAGA !","Uruchomiono skrypt z komponentu lub biblioteki !" & @CRLF & "[TAK] - uruchom skrypt" & @CRLF & "[NIE] - przerwij działanie i zakończ skrypt")
	If $Utils_iMsgBoxAnswer = 7 Then Exit;No
; od tego miejsca wykonywanie skryptu






EndIf
